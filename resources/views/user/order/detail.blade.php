@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
		table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{
			color: transparent;
		}
		table.detail-design>tbody>tr>th, table.detail-design>tbody>tr>td{
			border:none;
			padding: 2px 5px;
		}
		ul.history-order{
			list-style: none;
			padding: 0;
			margin: 0;
		}
		ul.history-order>li{
			float: left;
			margin-right: 5px;
		}
		ul.history-order>li:first-child{
			min-width: 120px
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading"><a href="{{route('user.order.list')}}"><i class="fa fa-angle-left"></i> Kembali</a>
		                <div class="pull-right">Detail Order</div>
		            </div>

		            <form method="POST" action="{{route('user.order.update', ['id'=>$order->id])}}">
                        {{ csrf_field() }}
		            	<div class="panel-body">
		                    <table class="table detail-design" style="margin-bottom: 0">
	                            <tr>
	                                <th width="200">Kode Order</th>
	                                <th width="1">:</th>
	                                <th>{{$order->order_code}}</th>
	                            </tr>
	                            <tr>
	                                <th>Nama Pemesan</th>
	                                <th>:</th>
	                                <td>{{$order->name}}</td>
	                            </tr>
	                            <tr>
	                                <th>Email UII</th>
	                                <th>:</th>
	                                <td>{{$order->email}}</td>
	                            </tr>
	                            <tr>
	                                <th>Nomor Kontak Pemesan</th>
	                                <th>:</th>
	                                <td>{{$order->contact}}</td>
	                            </tr>
	                            <tr>
	                                <th>Fakultas/ Badan/ Direktorat</th>
	                                <th>:</th>
	                                <td>{{$order->fbd}}</td>
	                            </tr>
	                            <tr>
	                                <th>Jurusan/ Departemtn/ Divisi</th>
	                                <th>:</th>
	                                <td>{{$order->jdd}}</td>
	                            </tr>
	                            <tr>
	                                <th>Deadline yang diinginkan</th>
	                                <th>:</th>
	                                <td>
	                                    <?php echo date('d F Y', strtotime($order->deadline)); ?>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Jenis Layanan Desain</th>
	                                <th>:</th>
	                                <td>
	                                    <select class="form-control input-sm" name="design_service">
	                                        <option value="{{$order->design_id}}">{{$order->design}}</option>
	                                        @foreach($services as $service)
	                                            @if($service->id != $order->design_service_id)
	                                                <option value="{{$service->id}}">{{$service->service}}</option>
	                                            @endif
	                                        @endforeach
	                                    </select>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Detail dan catatan Pemesan</th>
	                                <th>:</th>
	                                <td></td>
	                            </tr>
	                            <tr>
	                                <td colspan="3">
	                                    <div class="panel panel-default">
	                                        <div class="panel-body">
	                                            {!! $order->note !!}
	                                        </div>
	                                    </div>
	                                </td>
	                            </tr>
	                        </table>
	                        <hr>
	                        @if($order->designer_id)
		                        @foreach($histories as $history)
		                            <ul class="history-order">
		                                <li>
		                                    <?php echo date('d F Y', strtotime($history->created_at)); ?>
		                                </li>
		                                <li>
		                                    {{ $history->status_act }}
		                                </li>
		                                <li>
		                                    Oleh {{$history->designer}}
		                                </li>
		                            </ul>
		                            <br>
		                        @endforeach
		                     @endif
	                        <hr>
	                        @if($order->order_status_id==1)
	                        	<button type="submit" value="2" name="update_status" class="btn btn-sm btn-warning">Kerjakan</button>

	                        @elseif($order->order_status_id==2)
	                        	@if($order->designer_id==Auth::user()->id)
		                        	<div class="form-group">
		                        		<label class="control-label">Tautan Hasil Desain:</label>
		                        		<input type="text" name="url_attach" class="form-control input-sm" value="{{$order->url_attach}}">
		                        	</div>
		                        	<button type="submit" value="3" name="update_status" class="btn btn-sm btn-primary">Ajukan</button>
		                        @endif

	                        @elseif($order->order_status_id==3)
	                        	@if(Auth::user()->role=='Admin')
		                        	<div class="form-group">
		                        		<label class="control-label">Tautan Hasil Desain:</label>
		                        		<input type="text" name="url_attach" class="form-control input-sm" value="{{$order->url_attach}}">
		                        	</div>
		                        	<button type="submit" value="4" name="update_status" class="btn btn-sm btn-success">Selesai</button>
		                        @endif
	                        	@if($order->designer_id==Auth::user()->id)
		                        	<div class="form-group">
		                        		<label class="control-label">Tautan Hasil Desain:</label>
		                        		<input type="text" name="url_attach" class="form-control input-sm" value="{{$order->url_attach}}">
		                        	</div>
		                        	<button type="submit" value="" name="update_status" class="btn btn-sm btn-primary">Ganti Tautan</button>
		                        @endif

	                        @elseif($order->order_status_id==4)
	                        	@if($order->designer_id==Auth::user()->id || Auth::user()->role=='Admin')
		                        	<div class="form-group">
		                        		<label class="control-label">Tautan Hasil Desain:</label>
		                        		<input type="text" name="url_attach" class="form-control input-sm" value="{{$order->url_attach}}">
		                        	</div>
		                        	<button type="submit" value="" name="update_status" class="btn btn-sm btn-primary">Ganti Tautan</button>
		                        @endif
	                        @endif
			            </div>
		            </form>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"autoWidth":true,
				"order":false,
		        "info": false,
			});
		});
	</script>

@endsection

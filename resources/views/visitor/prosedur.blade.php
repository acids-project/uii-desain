@extends('visitor.app')

@section('title') Desain UII @endsection

@section('style')
    <style type="text/css">
        .img-prosedur img{
            max-width: 100%;
            max-height: 300px;
            display: block;
            margin: auto;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Prosedur</div>

            <div class="panel-body">
            	<h3>{{$data['main_title']}}</h3>
            	<p>{!!$data['main_content']!!}</p>
            	@foreach($data['list'] as $list)
            		<h4>{{$list['title']}}</h4>
            		<p>{!! $list['content'] !!}</p>
            	@endforeach

                <div class="img-prosedur">
                    <img src="{{asset($data['img_path'])}}">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sctipt')

@endsection

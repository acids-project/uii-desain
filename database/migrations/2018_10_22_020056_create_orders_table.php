<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_code')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('fbd')->nullable();
            $table->string('jdd')->nullable();
            $table->string('deadline')->nullable();
            $table->text('note')->nullable();
            $table->string('attach_path')->nullable();
            $table->string('design_service_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('url_attach')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

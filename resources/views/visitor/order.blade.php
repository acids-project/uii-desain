@extends('visitor.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" integrity="sha256-JDBcnYeV19J14isGd3EtnsCQK05d8PczJ5+fvEvBJvI=" crossorigin="anonymous" />
	<style type="text/css">
		.btn-order-submit{
			background-color: #06337B;
			color: white;
		}
		.btn-order-submit:hover{
			background-color: #052E70;
			color: white;
		}
	</style>
@endsection

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Order</div>

            <div class="panel-body">
				<form method="POST" action="{{route('visitor.store.order')}}">
                    {{ csrf_field() }}

                    <div class="row">
                    	<div class="col-xs-12 col-md-6">
                    		<div class="form-group">
                    			<label for="nama-pemesan">Nama Pemesan</label>
                    			<input type="text" name="name" class="form-control input-sm" id="nama-pemesan" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="email">Email UII</label>
                    			<input type="email" name="email" class="form-control input-sm" id="email" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="kontak">Nomor Kontak Pemesan</label>
                    			<input type="text" name="contact" class="form-control input-sm" id="kontak" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="direktorat">Fakultas/ Badan/ Direktorat</label>
                    			<input type="text" name="fbd" class="form-control input-sm" id="direktorat" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="divisi">Jurusan/ Departemen/ Divisi</label>
                    			<input type="text" name="jdd" class="form-control input-sm" id="divisi" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="deadline">Deadline yang diinginkan</label>
                    			<input type="text" name="deadline" class="form-control input-sm" id="deadline" required="">
                    		</div>
                    		<div class="form-group">
                    			<label for="jenis">Jenis Layanan Desain</label>
                    			<select type="text" name="designSevices" class="form-control input-sm" id="jenis" required="">
                    				<option value="">-- Pilih Jenis Layanan Desain --</option>
                    				@foreach($services as $service)
                        				<option value="{{$service->id}}">{{$service->service}}</option>
                    				@endforeach
                    			</select>
                    		</div>
                    	</div>
                    	<div class="col-xs-12 col-md-6">
                    		<div class="form-group">
                    			<label for="note">Detail dan catatan pemesanan</label>
                    			<textarea name="note" class="form-control input-sm textarea" id="note" rows="10" style="resize: none;"></textarea>
                    		</div>
                    		<div class="form-group">
                    			<label for="attach">File Pendukung (Maksimal 25MB) <br><small>Kompress semua file jika terdapat lebih dari satu file</small></label>
                    			<input type="file" name="file_attach" class="input-sm" id="attach">
                    		</div>
                    		<div class="form-group">
                    			<label for="attach">Dengan menekan tombol "Order Desain" saya menyatakan bahwa informasi yang dimasukkan telah memenuhi syarat dan ketentuan yang berlaku<br><small>* Informasi order akan disampaikan melalui email</small></label>
                    			<input type="submit" class="btn btn-sm btn-order-submit" value="Order Desain">
                    		</div>
                    	</div>
                    </div>
            	</form>
            </div>
        </div>
    </div>
@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js" integrity="sha256-tW5LzEC7QjhG0CiAvxlseMTs2qJS7u3DRPauDjFJ3zo=" crossorigin="anonymous"></script>
    <script src='https://devpreview.tiny.cloud/demo/tinymce.min.js'></script>
	<script type="text/javascript">
		$(function(){

            $('.navbar').css('z-index', '-1');

			tinymce.init({
                selector: '.textarea',
                  plugins: "textcolor code",
  toolbar: "forecolor code",
  textcolor_cols: "5"
              });
			//Date picker
			$('#deadline').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				todayHighlight:true,
			});

		});
	</script>

@endsection

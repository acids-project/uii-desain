@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
		table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{
			color: transparent;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Order Desain
		                <div class="pull-right">
		                    <span><i class="fa fa-angle-left"></i> 2018 <i class="fa fa-angle-right"></i></span>
		                    <span><i class="fa fa-angle-left"></i> September <i class="fa fa-angle-right"></i></span>
		                </div>
		            </div>

		            <div class="panel-body">
	                    <table class="table order-list" id="dataTable">
	                    	<thead>
	                    		<tr>
	                    			<th>Kode Order</th>
	                    			<th>Pemesan</th>
	                    			<th>Jenis Desain</th>
	                    			<th>Deadline</th>
	                    			<th>Status</th>
	                    			<th>Designer</th>
	                    			<th>Tautan</th>
	                    		</tr>
	                    	</thead>
	                        <tbody>
	                            @foreach($orders as $order)
	                                <tr>
	                                    <th>
	                                        <a href="{{route('user.order.detail', ['id'=>$order->id])}}">{{$order->order_code}}</a>
	                                    </th>
	                                    <td>{{$order->name}}</td>
	                                    <td>{{$order->design}}</td>
	                                    <td>
	                                        <?php echo date('d M Y', strtotime($order->deadline)); ?>
	                                    </td>
	                                    <td>
	                                        <label class="label {{$order->class}}">{{$order->status}}</label>
	                                    </td>
	                                    <td>
	                                        {{$order->designer}}
	                                    </td>
	                                    <td>
	                                        @if($order->url_attach)
	                                            <a href="#">Tatutan</a>
	                                        @endif
	                                    </td>
	                                </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
		            </div>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"autoWidth":true,
				"order":false,
		        "info": false,
			});
		});
	</script>
@endsection

<?php

namespace App\Http\Controllers\Visitor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

use App\Models\Order\Order;
use App\Models\Order\OrderStatus;
use App\Models\Order\OrderHistory;
use App\Models\Order\DesignService;

use App\Models\Media;

use DB;
use App\User;
use Carbon\Carbon;

class VisitorController extends Controller
{
    public function index()
    {
        return view('visitor.index');
    }

    public function prosedur()
    {
        $file = public_path().'/app/json/prosedur.json';
        $jsonString = file_get_contents($file);
        $data = json_decode($jsonString, true);

        return view('visitor.prosedur')
                    ->with('data', $data);
    }

    // public function prosedur3()
    // {
    //     $file = public_path().'/app/json/prosedur.json';
    //     $jsonString = file_get_contents($file);
    //     $data = json_decode($jsonString, true);

    //     $data['main_title'] = "Prosedur";
    //     $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
    //     file_put_contents($file, stripslashes($newJsonString));

    //     return view('visitor.prosedur')
    //                 ->with('data', $data);
    // }

    public function kontak()
    {
        return view('visitor.kontak');
    }

    public function branding()
    {
        return view('visitor.branding');
    }

    public function order()
    {
        $services = DesignService::all();

        return view('visitor.order')
                ->with('services', $services);
    }

    public function storeOrder(Request $request)
    {
        $emailUii= basename($request['email'], '@uii.ac.id');
        $strPost = strpos($emailUii, "@");
        if ($strPost) {
            return redirect()->back()->withInput();
        }

        $orderCode  ="";
        $dt = Carbon::now()->toDateString();
        $preOid = str_replace("-", "", $dt);

        $currentOrder = Order::where('order_code', 'like', '%'.$preOid.'%')->orderBy('id', 'DISC')->first();

        if ($currentOrder) {
            $pre = substr($currentOrder->order_code, 9);
            $number = (int)$pre+1;
            $orderCode = $preOid."-".$number;
        }else{
            $orderCode = $preOid."-1";
        }

        $preDeadline=str_replace('/', '-', $request['deadline']);
        $deadline = date("Y-m-d", strtotime($preDeadline));

        $order = new Order();
        $order->order_code=$orderCode;
        $order->name=$request['name'];
        $order->email=$request['email'];
        $order->contact=$request['contact'];
        $order->fbd=$request['fbd'];
        $order->jdd=$request['jdd'];
        $order->deadline=$deadline;
        $order->design_service_id=$request['designSevices'];
        $order->note=$request['note'];
        $order->save();
        
        return redirect()->route('visitor.resume.order');
    }

    public function resumeOrder()
    {
        return view('visitor.resume-order');
    }

}

<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Content\PageContent;
use App\Models\Content\FileContent;

class WebContentController extends Controller
{
    public function beranda()
    {
        return view('user.page.beranda');
    }
    public function berandaPost(Request $request)
    {


        return redirect()->route('page.beranda');
    }

    public function prosedur()
    {
        $images = FileContent::where('page', 2)->get();
        $prosedur = PageContent::find(2);
        return view('user.page.prosedur')
                ->with('prosedur', $prosedur)
                ->with('images', $images);
    }
    public function prosedurPost(Request $request)
    {
        $prosedur = PageContent::find(2);
        $prosedur->content=$request['content'];
        $prosedur->update();

        if($request->file('gallery')){
            foreach ($request->file('gallery') as $value) {
                $uidimg = uniqid();
                $file       = $value;
                $fileName   = $file->getClientOriginalName();
                $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                $pindah = $value->move("images/page/", $uidimg.'.'.$ext);
                if ($pindah) {
                  $gallery = new FileContent();
                  $gallery->page = 2;
                  $gallery->type_of_file="images";
                  $gallery->url_path="images/page/".$uidimg.'.'.$ext;
                  $gallery->save();
                }
            }
        }

        return redirect()->route('page.prosedur');
    }

    public function contact()
    {
        return view('user.page.kontak');
    }
    public function contactPost(Request $request)
    {


        return redirect()->route('page.contact');
    }

    public function branding()
    {
        return view('user.page.branding');
    }
    public function brandingPost(Request $request)
    {


        return redirect()->route('page.branding');
    }

    public function deleteImgContent(Request $request)
    {
        $this->id = $request['id'];
        $images = FileContent::find($this->id);
        $images->delete();

        $json['id'] = $this->id;

        return json_encode($json);
    }
}

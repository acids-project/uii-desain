@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')

    		<?php $plusYear = $year+1; $minusYear = $year-1 ?>
    		<?php $plusMonth = date("m", strtotime("+1 month", strtotime($val))); ?>
    		<?php $minusMonth = date("m", strtotime("-1 month", strtotime($val))); ?>


    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Statistik
		            	<div class="pull-right">
		            		<span>
		            			<a href="{{route('user.order.statistik', ['year'=>$minusYear, 'month'=>$month])}}"><i class="fa fa-angle-left"></i></a>
		            			{{$year}}
		            			<a href="{{route('user.order.statistik', ['year'=>$plusYear, 'month'=>$month])}}"><i class="fa fa-angle-right"></i></a>
		            		</span>
		            		<span>
		            			<a href="{{route('user.order.statistik', ['year'=>$year, 'month'=>$minusMonth])}}"><i class="fa fa-angle-left"></i></a>
		            			{{$monthName}}
		            			<a href="{{route('user.order.statistik', ['year'=>$year, 'month'=>$plusMonth])}}"><i class="fa fa-angle-right"></i></a>
		            		</span>
		            	</div>
		            </div>

		            <div class="panel-body">
		            	<center>Bulan {{$monthName}}</center>
		            	<table class="table">
		            		<thead>
		            			<tr>
		            				<th>Nama</th>
			            			@foreach($services as $service)
			            				<th>{{$service->service}}</th>
			            			@endforeach	
			            			<th>Total</th>
		            			</tr>
		            		</thead>
		            		<tbody>
		            			<tr>
		            				<td>Layanan Desain UII</td>
		            				<?php $totalDesainMonth = 0; ?>
			            			@foreach($totalMonth as $month)
			            				<?php $totalDesainMonth+=$month->total;?>
			            				<td>{{$month->total}}</td>
			            			@endforeach	
			            			<td>{{$totalDesainMonth}}</td>
		            			</tr>
	            				@foreach($resumeDesignerMonth as $resumeMonth)
	            					<?php $total=0; ?>
	            					<tr>
	            						<td>{{$resumeMonth->name}}</td>
				            			@foreach($services as $service)
				            				<?php $using = '_'.$service->id;?>
			            					<?php $total+=$resumeMonth->$using; ?>
				            				<td>{{$resumeMonth->$using}}</td>
				            			@endforeach
				            			<td>{{$total}}</td>
	            					</tr>
	            				@endforeach
		            		</tbody>
		            	</table>

		            	<center>Tahun {{$year}}</center>
		            	<table class="table">
		            		<thead>
		            			<tr>
		            				<th class="contoh">Nama</th>
			            			@foreach($services as $service)
			            				<th>{{$service->service}}</th>
			            			@endforeach	
			            			<th>Total</th>
		            			</tr>
		            		</thead>
		            		<tbody>
		            			<tr>
		            				<td>Layanan Desain UII</td>
		            				<?php $totalDesainYear = 0; ?>
			            			@foreach($totalYear as $year)
			            				<?php $totalDesainYear+=$year->total;?>
			            				<td>{{$year->total}}</td>
			            			@endforeach	
			            			<td>{{$totalDesainYear}}</td>
		            			</tr>
	            				@foreach($resumeDesignerYear as $resumeYear)
	            					<?php $total=0; ?>
	            					<tr>
	            						<td>{{$resumeYear->name}}</td>
				            			@foreach($services as $service)
				            				<?php $using = '_'.$service->id;?>
			            					<?php $total+=$resumeYear->$using; ?>
				            				<td>{{$resumeYear->$using}}</td>
				            			@endforeach
				            			<td>{{$total}}</td>
	            					</tr>
	            				@endforeach
		            		</tbody>
		            	</table>
		            </div>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('sctipt')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.contoh').css('transform', 'rotate(30deg)');
		})
	</script>
@endsection

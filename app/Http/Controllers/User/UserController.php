<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

use App\Models\Order\Order;
use App\Models\Order\OrderStatus;
use App\Models\Order\OrderHistory;
use App\Models\Order\DesignService;

Use DB;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;

        $allFinish = Order::where('order_status_id', 4)->where('user_id', Auth::user()->id)->count();
        $yearFinish = Order::where('order_status_id', 4)->whereYear('updated_at', $year)->where('user_id', Auth::user()->id)->count();
        $monthFinish = Order::where('order_status_id', 4)->whereMonth('updated_at', $month)->where('user_id', Auth::user()->id)->count();

        return view('user.index')
                ->with('allFinish', $allFinish)
                ->with('yearFinish', $yearFinish)
                ->with('monthFinish', $monthFinish);
    }

    public function listOrder()
    {
        $orders = DB::table('orders')
                    ->join('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')
                    ->join('design_services', 'orders.design_service_id', '=', 'design_services.id')
                    ->leftJoin('users', 'orders.user_id', '=', 'users.id')
                    ->select(
                        'orders.*',
                        'design_services.service as design',
                        'order_statuses.status',
                        'order_statuses.class',
                        'users.name as designer'
                    )
                    ->orderBy('orders.id', 'DESC')
                    ->get();

        return view('user.order.index')
                ->with('orders', $orders);
    }

    public function detailOrder($id)
    {
        $histories="";
        $services = DesignService::all();
        $order = DB::table('orders')
                    ->join('order_statuses', 'orders.order_status_id', '=', 'order_statuses.id')
                    ->join('design_services', 'orders.design_service_id', '=', 'design_services.id')
                    ->leftJoin('users', 'orders.user_id', '=', 'users.id')
                    ->select(
                        'orders.*',
                        'design_services.service as design',
                        'design_services.id as design_id',
                        'order_statuses.status',
                        'order_statuses.class',
                        'users.id as designer_id',
                        'users.name as designer'
                    )
                    ->where('orders.id', $id)
                    ->first();

        if ($order->designer_id) {
            $histories = DB::table('order_histories')
                        ->join('users', 'order_histories.user_id', 'users.id')
                        ->join('order_statuses', 'order_histories.order_status_id', 'order_statuses.id')
                        ->select(
                            'order_histories.*',
                            'users.name as designer',
                            'order_statuses.status_act'
                        )
                        ->where('order_id', $id)
                        ->get();
        }

        return view('user.order.detail')
                ->with('order', $order)
                ->with('services', $services)
                ->with('histories', $histories);
    }

    public function updateOrder(Request $request, $id)
    {
        $order = Order::find($id);
        $order->design_service_id = $request['design_service'];

        if ($request['update_status']) {
            $order->order_status_id = $request['update_status'];
        }

        if (empty($order->user_id)) {
            $order->user_id = Auth::user()->id;
        }
        
        $order->url_attach=$request['url_attach'];
        $order->update();

        if ($request['update_status']) {
            $history = new OrderHistory();
            $history->order_status_id = $request['update_status'];
            $history->order_id=$id;
            $history->user_id = Auth::user()->id;
            $history->save();
        }

        return redirect()->route('user.order.detail', ['id'=>$id]);
    }

    // Admin Function
    public function listDesigner()
    {
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;

        $designers = DB::table('users')
                    ->leftJoin('orders', 'users.id', '=', 'orders.user_id')
                    ->select(
                        'users.id',
                        'users.name',
                        'users.role',
                        DB::raw('COUNT(orders.id) as total')
                    )
                    ->groupBy('users.id','users.name', 'users.role')
                    ->get();

        return view('user.designer.list')
                ->with('designers', $designers);
    }
    public function createDesigner()
    {
        return view('user.designer.create');
    }
    public function storeDesigner(Request $request)
    {
        $user = new User();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->phone=$request['contact'];
        $user->role=$request['role'];
        $user->password=Hash::make('supersekali');
        $user->save();

        return view('user.designer.list');
    }
    public function updateRoleDesigner($id, $role)
    {
        if ($role!="Admin" && $role!="Designer") {
            return redirect()->route('user.list.designer');
        }

        $user = User::find($id);
        $user->role=$role;
        $user->update();

        return redirect()->route('user.list.designer');
    }

    public function statistik($year, $month)
    {
        $this->year = $year;
        $this->month = $month;

        $monthName = date('F', mktime(0, 0, 0, $month, 10));
        $val = $year.'-'.$month.'-01';

        $services = DesignService::orderBy('id', 'ASC')->get();
        $totalMonth = DB::table('design_services')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.design_service_id', 'design_services.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->whereMonth('orders.updated_at', $this->month)
                            ->where('orders.order_status_id', 4);
                    })
                    ->select(
                        'service',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('service')
                    ->orderBy('design_services.id', 'ASC')
                    ->get();

        $totalYear = DB::table('design_services')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.design_service_id', 'design_services.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->where('orders.order_status_id', 4);
                    })
                    ->select(
                        'service',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('service')
                    ->orderBy('design_services.id', 'ASC')
                    ->get();

        $designerMonth = DB::table('users')
                        ->leftJoin('orders', function($leftJoin)
                        {
                            $leftJoin->on('orders.user_id', 'users.id')
                                ->whereYear('orders.updated_at', $this->year)
                                ->whereMonth('orders.updated_at', $this->month)
                                ->where('orders.order_status_id', 4);
                        })
                        ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id');
        $rawMonth[]='users.id';
        $rawMonth[]='users.name';
        foreach ($services as $value) {
            $rawMonth[]=DB::raw("SUM(CASE WHEN design_services.service = '$value->service' THEN 1 ELSE 0 END) as '_$value->id'");
        }
        $designerMonth->select($rawMonth);
        $resumeDesignerMonth = $designerMonth->groupBy('users.id')->get();

        $designerYear = DB::table('users')
                        ->leftJoin('orders', function($leftJoin)
                        {
                            $leftJoin->on('orders.user_id', 'users.id')
                                ->whereYear('orders.updated_at', $this->year)
                                ->where('orders.order_status_id', 4);
                        })
                        ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id');
        $rawYear[]='users.id';
        $rawYear[]='users.name';
        foreach ($services as $value) {
            $rawYear[]=DB::raw("SUM(CASE WHEN design_services.service = '$value->service' THEN 1 ELSE 0 END) as '_$value->id'");
        }
        $designerYear->select($rawYear);
        $resumeDesignerYear = $designerYear->groupBy('users.id')->get();

        return view('user.statistik2')
                    ->with('val', $val)
                    ->with('year', $this->year)
                    ->with('month', $this->month)
                    ->with('monthName', $monthName)
                    ->with('totalYear', $totalYear)
                    ->with('totalMonth', $totalMonth)
                    ->with('services', $services)
                    ->with('resumeDesignerYear', $resumeDesignerYear)
                    ->with('resumeDesignerMonth', $resumeDesignerMonth);
    }

    public function statistik2($year, $month)
    {
        $this->year = $year;
        $this->month = $month;

        $monthName = date('F', mktime(0, 0, 0, $month, 10));
        $val = $year.'-'.$month.'-01';

        $totalMonth = DB::table('design_services')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.design_service_id', 'design_services.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->whereMonth('orders.updated_at', $this->month)
                            ->where('orders.order_status_id', 4);
                    })
                    ->select(
                        'service',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('service')
                    ->get();

        $totalYear = DB::table('design_services')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.design_service_id', 'design_services.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->where('orders.order_status_id', 4);
                    })
                    ->select(
                        'service',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('service')
                    ->get();

        $designerTotalMonth =  DB::table('users')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.user_id', 'users.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->whereMonth('orders.updated_at', $this->month)
                            ->where('orders.order_status_id', 4);
                    })
                    ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id')
                    ->select(
                        'users.name',
                        'users.id',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('users.name', 'users.id')
                    ->get();

        $designerTotalYear =  DB::table('users')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.user_id', 'users.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->where('orders.order_status_id', 4);
                    })
                    ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id')
                    ->select(
                        'users.name',
                        'users.id',
                        DB::raw('count(orders.id) as total')
                    )
                    ->groupBy('users.name', 'users.id')
                    ->get();

        $designerListYear =  DB::table('users')
                    ->leftJoin('orders', function($leftJoin)
                    {
                        $leftJoin->on('orders.user_id', 'users.id')
                            ->whereYear('orders.updated_at', $this->year)
                            ->where('orders.order_status_id', 4);
                    })
                    ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id')
                    ->select(
                        'users.name',
                        'users.id',
                        'design_services.service',
                        DB::raw('count(orders.id) as total'),
                        DB::raw('count(design_services.id) as totalService')
                    )
                    ->groupBy('users.id', 'design_services.service')
                    ->get();

        $services = DesignService::all();
        $kerjaUserYear = DB::table('users')
                        ->leftJoin('orders', function($leftJoin)
                        {
                            $leftJoin->on('orders.user_id', 'users.id')
                                ->whereYear('orders.updated_at', $this->year)
                                ->where('orders.order_status_id', 4);
                        })
                        ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id');
        $data=[];
        $data[]='users.id';
        $data[]='users.name';
        foreach ($services as $value) {
                $data[]=DB::raw("SUM(CASE WHEN design_services.service = '$value->service' THEN 1 ELSE 0 END) as '_$value->id'");
            }
        $kerjaUserYear->select($data);
        $resultYear = $kerjaUserYear->groupBy('users.id')->get();

        $kerjaUserMonth = DB::table('users')
                        ->leftJoin('orders', function($leftJoin)
                        {
                            $leftJoin->on('orders.user_id', 'users.id')
                                ->whereYear('orders.updated_at', $this->year)
                                ->whereMonth('orders.updated_at', $this->month)
                                ->where('orders.order_status_id', 4);
                        })
                        ->leftJoin('design_services', 'orders.design_service_id', '=', 'design_services.id')
                        ->select(
                            'users.id',
                            'users.name',
                            DB::raw("SUM(CASE WHEN design_services.service = 'Baliho' THEN 1 ELSE 0 END) as baliho"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Banner' THEN 1 ELSE 0 END) as banner"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Booklet' THEN 1 ELSE 0 END) as booklet"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Brosur' THEN 1 ELSE 0 END) as brosur"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Iklan Koran' THEN 1 ELSE 0 END) as iklan"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Kartu Nama' THEN 1 ELSE 0 END) as kartu_nama"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Poster' THEN 1 ELSE 0 END) as poster"),
                            DB::raw("SUM(CASE WHEN design_services.service = 'Sampul' THEN 1 ELSE 0 END) as sampul")
                        )
                        ->groupBy('users.id')
                        ->get();
        // dd($kerjaUserYear);

        return view('user.statistik')
                    ->with('resultYear', $resultYear)
                    ->with('services', $services)
                    ->with('val', $val)
                    ->with('year', $year)
                    ->with('month', $month)
                    ->with('totalYear', $totalYear)
                    ->with('monthName', $monthName)
                    ->with('totalMonth', $totalMonth)
                    ->with('kerjaUserYear', $kerjaUserYear)
                    ->with('kerjaUserMonth', $kerjaUserMonth)
                    ->with('designerTotalYear', $designerTotalYear)
                    ->with('designerTotalMonth', $designerTotalMonth);
    }
}

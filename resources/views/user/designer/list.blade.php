@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
		table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{
			color: transparent;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">List Designer</div>

		            <div class="panel-body">
	                    <table class="table order-list" id="dataTable">
	                    	<thead>
	                    		<tr>
	                    			<th>Nama Desainer</th>
	                    			<th>Status Designer</th>
	                    			<th></th>
	                    		</tr>
	                    	</thead>
	                        <tbody>
	                        	@foreach($designers as $designer)
	                        		<tr>
	                        			<td>{{$designer->name}}</td>
	                        			<td>{{$designer->role}}</td>
	                        			<td>
	                        				@if($designer->role=="Designer")
	                        					<a href="{{route('user.update.role.designer', ['id'=>$designer->id, 'role'=>'Admin'])}}"> <small>Jadikan Admin</small></a>
	                        				@elseif($designer->role=="Admin" && $designer->id!=Auth::user()->id)
	                        					<a href="{{route('user.update.role.designer', ['id'=>$designer->id, 'role'=>'Designer'])}}"> <small>Jadikan Designer</small></a>
	                        				@endif
	                        			</td>
	                        		</tr>
	                        	@endforeach
	                        </tbody>
	                    </table>
		            </div>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"autoWidth":false,
				"order":false,
		        "info": false,
			});
		});
	</script>
@endsection

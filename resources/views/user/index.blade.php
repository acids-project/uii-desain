@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Profil</div>

		            <div class="panel-body">
		            	<h4 style="margin: 0; padding: 0">{{Auth::user()->name}}</h4>
		            	<p>{{Auth::user()->role}} Direktorat Pemasaran @if(Auth::user()->note) ({{Auth::user()->note}}) @endif</p>
		            	<hr>
		            	<h4 style="margin: 0; padding: 0">Statistik</h4>
		            	<p>Desain Keseluruhan yang diselesaikan : <b>{{$allFinish}}</b></p>
		            	<p>Desain yang diselesaikan tahun ini : <b>{{$yearFinish}}</b></p>
		            	<p>Desain yang diselesaikan bulan ini : <b>{{$monthFinish}}</b></p>
		            	<hr>
		            	<form method="POST">
		            		<div class="row">
		            			<div class="col-md-6">
					            	<h4 style="margin: 0; padding: 0">Ganti Password</h4>
		            				<div class="form-group">
		            					<label class="control-label">Password Lama</label>
		            					<input type="password" name="" class="form-control input-sm">
		            				</div>
		            				<div class="form-group">
		            					<label class="control-label">Password Baru</label>
		            					<input type="password" name="" class="form-control input-sm">
		            				</div>
		            				<div class="form-group">
		            					<label class="control-label">Ulangi Password Baru</label>
		            					<input type="password" name="" class="form-control input-sm">
		            				</div>
		            				<button type="button" class="btn btn-sm btn-uii" disabled="">Ganti Password</button>
		            			</div>
		            		</div>
		            	</form>
		            </div>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('sctipt')

@endsection

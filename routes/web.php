<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 					'Visitor\VisitorController@index')->name('visitor.index');
Route::get('/prosedur', 			'Visitor\VisitorController@prosedur')->name('visitor.prosedur');
Route::get('/kontak', 				'Visitor\VisitorController@kontak')->name('visitor.kontak');
Route::get('/branding-guidelines', 	'Visitor\VisitorController@branding')->name('visitor.branding');
Route::get('/order', 				'Visitor\VisitorController@order')->name('visitor.order');
Route::post('/store-order', 		'Visitor\VisitorController@storeOrder')->name('visitor.store.order');
Route::get('/resume-order', 		'Visitor\VisitorController@resumeOrder')->name('visitor.resume.order');

// User
Route::group(['prefix'=>'user', 'middleware'=>'auth'], function(){
	Route::get('/', 					'User\UserController@index')->name('user.index');
	Route::get('/list-order', 			'User\UserController@listOrder')->name('user.order.list');
	Route::get('/detail-order/{id}', 	'User\UserController@detailOrder')->name('user.order.detail');
	Route::post('/update-order/{id}', 	'User\UserController@updateOrder')->name('user.order.update');

	Route::get('/statistik/{year}/{month}', 	'User\UserController@statistik')->name('user.order.statistik');

	Route::group(['middleware'=>'Admin'], function(){
		Route::get('/list-designer', 						'User\UserController@listDesigner')->name('user.list.designer');
		Route::get('/create-designer', 						'User\UserController@createDesigner')->name('user.create.designer');
		Route::post('/store-designer', 						'User\UserController@storeDesigner')->name('user.store.designer');
		Route::post('/update-designer/{id}', 				'User\UserController@updateDesigner')->name('user.update.designer');
		Route::get('/update-role-designer/{id}/{role}', 	'User\UserController@updateRoleDesigner')->name('user.update.role.designer');
		Route::get('/show-designer/{id}', 					'User\UserController@showDesigner')->name('user.show.designer');

		// Web Content
		Route::get('/beranda', 			'User\WebContentController@beranda')->name('page.beranda');
		Route::post('/beranda-post', 	'User\WebContentController@berandaPost')->name('page.beranda.post');
		Route::get('/prosedur', 		'User\WebContentController@prosedur')->name('page.prosedur');
		Route::post('/prosedur-post', 	'User\WebContentController@prosedurPost')->name('page.prosedur.post');
		Route::get('/contact', 			'User\WebContentController@contact')->name('page.contact');
		Route::post('/contact-post', 	'User\WebContentController@contactPost')->name('page.contact.post');
		Route::get('/branding', 		'User\WebContentController@branding')->name('page.branding');
		Route::post('/branding-post', 	'User\WebContentController@brandingPost')->name('page.branding.post');
	});
});

Route::post('/delete/img-content', 'User\WebContentController@deleteImgContent');


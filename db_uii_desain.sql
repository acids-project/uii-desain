-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22 Okt 2018 pada 16.41
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_uii_desain`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `design_services`
--

CREATE TABLE `design_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `design_services`
--

INSERT INTO `design_services` (`id`, `service`, `created_at`, `updated_at`) VALUES
(1, 'Baliho', NULL, NULL),
(2, 'Banner', NULL, NULL),
(3, 'Booklet', NULL, NULL),
(4, 'Brosur', NULL, NULL),
(5, 'Iklan Koran', NULL, NULL),
(6, 'Kartu Nama', NULL, NULL),
(7, 'Poster', NULL, NULL),
(8, 'Sampul', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_22_020056_create_orders_table', 2),
(4, '2018_10_22_020218_create_order_histories_table', 2),
(5, '2018_10_22_020232_create_order_statuses_table', 2),
(6, '2018_10_22_020248_create_design_services_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fbd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jdd` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `attach_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `design_service_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_status_id` int(10) DEFAULT '1',
  `url_attach` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `name`, `email`, `contact`, `fbd`, `jdd`, `deadline`, `note`, `attach_path`, `design_service_id`, `user_id`, `order_status_id`, `url_attach`, `created_at`, `updated_at`) VALUES
(1, '20181022-1', 'nama pemesan', 'email@uii.ac.id', '12341234', 'fakultas', 'jurusan', '2018-10-24', '<p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\n<br></p>', NULL, '1', '1', 4, 'attach url', '2018-10-21 20:22:51', '2018-10-21 21:43:04'),
(2, '20181022-2', 'pesan 1', 'email1@uii.ac.id', 'nomor', 'fak', 'jur', '2018-10-31', NULL, NULL, '1', '2', 4, 'url attach', '2018-10-21 22:33:33', '2018-10-21 22:50:23'),
(3, '20181022-3', 'pesan dua', 'uii@uii.ac.id', 'asdf', 'adf', 'af', '2018-10-23', NULL, NULL, '6', '3', 4, 'tautan edit', '2018-10-21 22:54:17', '2018-10-21 22:57:54'),
(4, '20181022-4', 'pesen tiga', 'email@uii.ac.id', 'asdf', 'af', 'sadfs', '2018-10-24', NULL, NULL, '7', '3', 4, 'sdfg', '2018-10-21 22:58:55', '2018-10-21 22:59:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_histories`
--

CREATE TABLE `order_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_status_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `order_histories`
--

INSERT INTO `order_histories` (`id`, `order_status_id`, `order_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '2', '1', '1', '2018-10-21 21:28:04', '2018-10-21 21:28:04'),
(2, '3', '1', '1', '2018-10-21 21:35:48', '2018-10-21 21:35:48'),
(3, '4', '1', '1', '2018-10-21 21:37:58', '2018-10-21 21:37:58'),
(4, '2', '2', '2', '2018-10-21 22:38:17', '2018-10-21 22:38:17'),
(5, '3', '2', '2', '2018-10-21 22:38:28', '2018-10-21 22:38:28'),
(9, '4', '2', '1', '2018-10-21 22:43:33', '2018-10-21 22:43:33'),
(12, '2', '3', '3', '2018-10-21 22:55:15', '2018-10-21 22:55:15'),
(13, '3', '3', '3', '2018-10-21 22:56:01', '2018-10-21 22:56:01'),
(14, '4', '3', '1', '2018-10-21 22:57:54', '2018-10-21 22:57:54'),
(15, '2', '4', '3', '2018-10-21 22:59:05', '2018-10-21 22:59:05'),
(16, '3', '4', '3', '2018-10-21 22:59:11', '2018-10-21 22:59:11'),
(17, '4', '4', '1', '2018-10-21 22:59:20', '2018-10-21 22:59:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_act` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `status`, `status_act`, `class`, `created_at`, `updated_at`) VALUES
(1, 'Masuk', NULL, 'label-danger', NULL, NULL),
(2, 'Dikerjakan', 'Dikerjakan', 'label-warning', NULL, NULL),
(3, 'Persetujuan', 'Diajukan', 'label-primary', NULL, NULL),
(4, 'Selesai', 'Disetujui', 'label-success', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Designer',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_2`, `username`, `phone`, `note`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@uii.ac.id', NULL, NULL, NULL, NULL, 'Admin', '$2y$10$HylBvmLR7OmHZ5l1Aq8jC.XIkkl5k8AbqTXtJnnRrl1CMbkVYBj0u', 'Bvp4qt4EHXIoNqLpMZxcxxJI97rxJ2kCQzcFHAoxs058IMGozSf0qoS3V3de', '2018-10-21 20:25:49', '2018-10-21 20:25:49'),
(2, 'designer', 'designer1@uii.ac.id', NULL, NULL, NULL, NULL, 'Designer', '$2y$10$7kRHAntiaBafSvnhkLkAW.EfXb6T40C2RehH1oSCBk2udNCImU1Lm', 'tZYVcFeeRf7zAABKAopIY4165KgmuUgwjtMRtJJD1KcmloyCgX2LeGnS1MNJ', '2018-10-21 22:32:21', '2018-10-22 01:06:15'),
(3, 'designer 2', 'designer2@uii.ac.id', NULL, NULL, NULL, NULL, 'Designer', '$2y$10$xq0ktpRAR4CNNrHhxpf8NeDc3RLnGcqHCt2vgK9.DTl8Q6oOdjXA6', 'ytJQhg6bnSNgqdjmczEkKuqfDTusmL6h6whBIEKmxGUSCEDHa0uIEIkZJ1hg', '2018-10-21 22:53:51', '2018-10-21 22:53:51'),
(4, 'designer 3', 'designer3@uii.ac.id', NULL, NULL, 'kontak', NULL, 'Designer', '$2y$10$ZUm2bT362tpbKjrYT.tXLu.I/bLT2keJwxZQ30ohU5.2xX4kWLvuq', NULL, '2018-10-22 00:30:33', '2018-10-22 00:30:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `design_services`
--
ALTER TABLE `design_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_histories`
--
ALTER TABLE `order_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `design_services`
--
ALTER TABLE `design_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order_histories`
--
ALTER TABLE `order_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

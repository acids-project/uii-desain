@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Branding</div>

		            <form method="POST" action="{{route('page.branding.post')}}">
                        {{ csrf_field() }}
		            	<div class="panel-body">

			            </div>
		            </form>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"autoWidth":true,
				"order":false,
		        "info": false,
			});
		});
	</script>

@endsection

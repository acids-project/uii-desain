@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.gallery-prosedur{
			margin: 1em 0;
		}
		.img-prosedure img{
			height: 100px;
			cursor: pointer;
			padding: 5px;
			border:1px solid #ddd;
			border-radius: 5px;
		}
		.img-prosedure-db img{
			cursor: default !important;
		}
		.img-prosedure{
			display: inline-block;
			position: relative;
			float: left;
			margin-right: 10px;
			margin-bottom: 20px;
		}
		.remove-gellery, .remove-gellery-db{
			position: absolute;
		    box-shadow: 0 2px 5px rgba(0,0,0,.16),0 2px 10px rgba(0,0,0,.12);
		    padding: 1px 4px;
		    font-size: 12px;
		    top: -8px;
		    right: -8px;
		    background: white;
		    border-radius: 50%;
		    cursor: pointer;
		    color: red;
		    border: 1px solid red;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Prosedur</div>

		            <form method="POST" action="{{route('page.prosedur.post')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
		            	<div class="panel-body">
		            		<div class="form-group">
		            			<textarea name="content" class="form-control input-sm textarea" rows="20">{{$prosedur->content}}</textarea>
		            		</div>
		            		<div class="form-group">
		            			<label class="control-label">Ilustrasi Prosedur</label>
	            				<div class="row">
	            					<div class="col-xs-12">
	            						<div class="gallery-prosedur">
	            							<?php $ttlImage=0;?>
	            							@foreach($images as $key=>$image)
					            				<div class="img-prosedure img-prosedure-db img{{$image->id}}">
			                                        <button type="button" class="remove-gellery-db" value="{{$image->id}}"><i class="fa fa-times"></i></button>
					            					<img src="{{ asset($image->url_path) }}" class="img-item{{$key}}">
					            					<input type="file" name="gallery[]" class="img-file hide" id="img-item{{$key}}" accept="image/*">
					            				</div>
		            							<?php $ttlImage=$key;?>
				            				@endforeach
				            				<div class="img-prosedure">
		                                        <div class="remove-gellery hide"><i class="fa fa-times"></i></div>
				            					<img src="{{ asset('app/images/upload.png') }}" class="img-item{{$ttlImage+1}} img-item">
				            					<input type="file" name="gallery[]" class="img-file hide" id="img-item{{$ttlImage+1}}" accept="image/*">
				            				</div>
				            			</div>
	            					</div>
	            				</div>
				            			
		            		</div>
		            		<hr>
				            <div style="margin: auto">
				            	<button class="btn btn-success "><i class="fa fa-check"></i> Submit</button>
				            </div>
			            </div>
		            </form>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="{{asset('app/bower_components/tinymce/js/tinymce/tinymce.min.js')  }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){

			tinymce.init({ 
				selector:'textarea',
				plugins: "code textcolor colorpicker table",
				menubar: "edit insert view format table help",
				toolbar:" formatselect, bold,italic, underline, align,forecolor,undo, redo, code"
			});

            var itemGambar=<?php echo json_encode($ttlImage+1); ?>;
            function readURL(input, classes) {
                if (input.files) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.'+classes).attr({'src': e.target.result});
                        $('.'+classes).prev().removeClass('hide');
                    }
                    reader.readAsDataURL(input.files[0]);
                    if (classes=='img-item'+itemGambar) {
                        itemGambar++;
                        appendBaru();
                    }
                }
            }
            function appendBaru(){
                $('.gallery-prosedur').append(
                    '<div class="img-prosedure">'+
                        '<div class="remove-gellery hide"><i class="fa fa-times"></i></div>'+
    					'<img src="{{ asset("app/images/upload.png") }}" class="img-item'+itemGambar+' img-item">'+
    					'<input type="file" name="gallery[]" class="img-file hide" id="img-item'+itemGambar+'" accept="image/*">'+
    				'</div>');
            }
			$('.gallery-prosedur').on('click', '.img-item', function(){
				$(this).next().click();
			});
            $('.gallery-prosedur').on('change', '.img-file', function(){
                var classes = $(this).attr('id');
                readURL(this, classes);
            });
            $('.gallery-prosedur').on('click', '.remove-gellery', function(){
                $(this).parent().remove();
            });
            $('.gallery-prosedur').on('click', '.remove-gellery-db', function(e){
		        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var tipes = "POST";
				var my_url = "http://localhost/Acids/UII/desain-uii/public/delete/img-content"
		        $.ajax({
		            type: tipes,
		            url: my_url,
		            data: {_token: CSRF_TOKEN, id:$(this).val()},
		            dataType: 'json',
		            success: function (data) {
		                console.log(data);
		                $('.img'+data.id).remove();
	                },
			            error: function (data) {
		                console.log('Error:', data);
		            }
		        });
            });
		});
	</script>

@endsection

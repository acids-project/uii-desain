@extends('user.base.app')

@section('title') Desain UII @endsection

@section('style')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
	<style type="text/css">
		ul{
			list-style: none;
			padding: 0;
		}
		.btn-uii{
			color: white;
			background-color: #062B66;
		}
		.btn-uii:hover{
			color: white;
			background-color: #032458;
		}
		table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{
			color: transparent;
		}
	</style>
@endsection

@section('content')
    <div class="container">
    	<div class="row">
    		@include('user.base.sidebar')
    		<div class="col-xs-12 col-md-9">
    			<div class="panel panel-default">
		            <div class="panel-heading">Buat Designer</div>

		            <div class="panel-body">
		            	<form class="form-horizontal" method="POST" action="{{route('user.store.designer')}}">
	                        {{ csrf_field() }}
	                        <div class="form-group">
	                        	<label class="control-label col-md-3">Nama : </label>
	                        	<div class="col-md-7">
	                        		<input type="text" name="name" class="form-control input-sm" required="">
	                        	</div>
	                        </div>
	                        <div class="form-group">
	                        	<label class="control-label col-md-3">Email : </label>
	                        	<div class="col-md-7">
	                        		<input type="text" name="email" class="form-control input-sm" required="">
	                        	</div>
	                        </div>
	                        <div class="form-group">
	                        	<label class="control-label col-md-3">Kontak : </label>
	                        	<div class="col-md-7">
	                        		<input type="text" name="contact" class="form-control input-sm" required="">
	                        	</div>
	                        </div>
	                        <div class="form-group">
	                        	<label class="control-label col-md-3">Catatan : </label>
	                        	<div class="col-md-7">
	                        		<input type="text" name="note" class="form-control input-sm">
	                        	</div>
	                        </div>
	                        <div class="form-group">
	                        	<label class="control-label col-md-3">Jadikan Sebagai : </label>
	                        	<div class="col-md-7">
	                        		<select class="form-control input-sm" name="role">
	                        			<option value="Designer">Designer</option>
	                        			<option value="Admin">Admin</option>
	                        		</select>
	                        	</div>
	                        </div>
	                        <button type="submit" class="btn btn-sm btn-success">Submit</button>
		            	</form>
		            </div>
		        </div>
    		</div>
    	</div>
		        
    </div>
@endsection

@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"autoWidth":true,
				"order":false,
		        "info": false,
			});
		});
	</script>
@endsection


    		<div class="col-xs-12 col-md-3">
    			<div class="panel panel-default">
		            <div class="panel-heading">{{Auth::user()->name}}</div>
		            <div class="panel-body">
		            	<?php $year = date("Y"); ?>
		            	<?php $month = date("m"); ?>
		            	<ul>
		            		<li>
		            			<a href="{{route('user.index')}}">Profil</a>
		            		</li>
		            		<li><hr></li>
		            		<li>
		            			<a href="{{route('user.order.list')}}">Order Desain</a>
		            		</li>
			            		<li>
			            			<a href="{{route('user.order.statistik', ['year'=>$year, 'month'=>$month])}}">Statistik</a>
			            		</li>
		            		<li><hr></li>
		            		@if(Auth::user()->role=='Admin')
			            		<li>
			            			<a href="{{route('user.list.designer')}}">List Designer</a>
			            		</li>
			            		<li>
			            			<a href="{{route('user.create.designer')}}">Tambah Designer</a>
			            		</li>
			            		<li><hr></li>
			            		<!-- Web Content -->
			            		<li>
			            			<a href="{{route('page.beranda')}}">Beranda</a>
			            		</li>
			            		<li>
			            			<a href="{{route('page.prosedur')}}">Prosedur</a>
			            		</li>
			            		<li>
			            			<a href="{{route('page.contact')}}">Kontak</a>
			            		</li>
			            		<li>
			            			<a href="{{route('page.branding')}}">Branding Guidlines</a>
			            		</li>
			            		<li><hr></li>
		            		@endif
		            		<li>
		            			<a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
		            		</li>
		            	</ul>
		            </div>
		        </div>
    		</div>